package assignment

import GitHubService._

import scala.io.StdIn

import cats.effect.{ExitCode, IO, IOApp}


object App extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    val console = new Console[IO] {
      override def printLine(text: String): IO[Unit] = IO(println(text))
      override def readLine: IO[String] = IO(StdIn.readLine())
    }
    work(console).map(_ => ExitCode.Success)
  }

  def work(console: Console[IO]): IO[Unit] =
    for {
      _ <- console.printLine("Enter login: ")
      login <- console.readLine
      _ <- console.printLine("")
      _ <- mostPopularRepo(login) match {
         case Right(repo) =>
           getMorePopularRepos(login, repo).foldLeft(IO.pure())((io, repo) =>
             io.flatMap(_ =>
               console.printLine(s"Name: ${repo.name} Stars: ${repo.stargazers_count.toString}")))
         case Left(error) => console.printLine(error.toString)
      }
    } yield ()
}