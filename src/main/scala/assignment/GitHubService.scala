package assignment

import assignment.Models._
import io.circe
import io.circe.generic.semiauto.deriveDecoder
import io.circe.{Decoder, parser}
import scalaj.http.Http



object GitHubService {
  implicit val userDecoder: Decoder[User] = deriveDecoder[User]
  implicit val repoDecoder: Decoder[Repo] = deriveDecoder[Repo]

  val root = "https://api.github.com/"

  def mostPopularRepo(login: String): Either[circe.Error, Repo] = {
    getUserRepos(login) match {
      case Right(repos) => Right(repos.maxBy(_.stargazers_count))
      case Left(error) => Left(error)
    }
  }

  def getMorePopularRepos(login: String, userRepo: Repo): List[Repo] = {
    for {
      user <-
        getUserFollowers(login) match {
          case Right(list) => list
          case Left(error) => Nil
        }
      repo <-
        getUserRepos(user.login) match {
          case Right(list) => list
          case Left(error) => Nil
        } if repo.stargazers_count > userRepo.stargazers_count
    } yield repo
  }

  def getUserInfo[V](resource: String)(implicit decoder: Decoder[V]): Either[circe.Error, List[V]] = {
    val result = Http(root + resource).asString
    val data = result.body
    parser.decode[List[V]](data)
  }

  def getUserRepos(login: String): Either[circe.Error, List[Repo]] = {
    getUserInfo[Repo]("users/" + login + "/repos")
  }

  def getUserFollowers(login: String): Either[circe.Error, List[User]] = {
    getUserInfo[User]("users/" + login + "/followers")
  }
}
