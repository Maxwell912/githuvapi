package assignment

object Models {
  case class Repo(name: String, stargazers_count: Int)
  case class User(login: String)
}

